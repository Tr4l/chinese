export const contentData = {
    "words":
        {
            "一" : { "name": "一", "pinyin": "yī", "pos": "number", "meaning": "un" },
            "二": { "name": "二", "pinyin": "Èr", "pos": "number", "meaning": "deux (pour les numero)" },
            "两": { "name": "两", "pinyin": "liǎng", "pos": "number", "meaning": "deux (pour les quantités)" },
            "三": { "name": "三", "pinyin": "sān", "pos": "number", "meaning": "trois" },
            "四": { "name": "四", "pinyin": "sì", "pos": "number", "meaning": "quatre" },
            "五": { "name": "五", "pinyin": "wǔ", "pos": "number", "meaning": "cinq" },
            "六": { "name": "六", "pinyin": "liù", "pos": "number", "meaning": "six" },
            "七": { "name": "七", "pinyin": "qī", "pos": "number", "meaning": "sept" },
            "八": { "name": "八", "pinyin": "bā", "pos": "number", "meaning": "huit" },
            "九": { "name": "九", "pinyin": "jiǔ", "pos": "number", "meaning": "neuf" },
            "十": { "name": "十", "pinyin": "shí", "pos": "number", "meaning": "dix" },
            "百": { "name": "百", "pinyin": "bǎi", "pos": "number", "meaning": "cent" },
            "千": { "name": "千", "pinyin": "qiān", "pos": "number", "meaning": "mille" },
            "万": { "name": "万", "pinyin": "wàn", "pos": "number", "meaning": "dix mille" },
            "我": { "name": "我", "pinyin": "wǒ", "pos": "pronoun", "meaning": "je" },
            "你": { "name": "你", "pinyin": "nǐ", "pos": "pronoun", "meaning": "tu" },
            "您": { "name": "您", "pinyin": "nín", "pos": "pronoun", "meaning": "vous (de politesse)" },
            "他": { "name": "他", "pinyin": "tā", "pos": "pronoun", "meaning": "il" },
            "她": { "name": "她", "pinyin": "tā", "pos": "pronoun", "meaning": "elle", "root":"女" },
            "我们": { "name": "我们", "pinyin": "wǒmen", "pos": "pronoun", "meaning": "nous" },
            "你们": { "name": "你们", "pinyin": "nǐmen", "pos": "pronoun", "meaning": "vous" },
            "他们": { "name": "他们", "pinyin": "tāmen", "pos": "pronoun", "meaning": "ils" },
            "法国": { "name": "法国", "pinyin": "fǎguó", "pos": "proper noun", "meaning": "France", "category": "country" },
            "英国": { "name": "英国", "pinyin": "yīngguó", "pos": "proper noun", "meaning": "Angleterre", "category": "country" },
            "美国": { "name": "美国", "pinyin": "měiguó", "pos": "proper noun", "meaning": "U.S.A", "category": "country" },
            "意大利": { "name": "意大利", "pinyin": "yìdàlì", "pos": "proper noun", "meaning": "Italie", "category": "country" },
            "中国": { "name": "中国", "pinyin": "zhōngguó", "pos": "proper noun", "meaning": "Chine", "category": "country" },
            "人": { "name": "人", "pinyin": "rén", "pos": "noun", "meaning": "gens (personne)" },
            "笔": { "name": "笔", "pinyin": "bǐ", "pos": "noun", "meaning": "crayon", "category": "thing" },
            "白": { "name": "白", "pinyin": "bái", "pos": "noun", "meaning": "blanc", "category": "color" },
            "几": { "name": "几", "pinyin": "jǐ", "pos": "xxx", "meaning": "quelque/combien"},
            "是": { "name": "是", "pinyin": "shì", "pos": "verb", "meaning": "être"},
            "学": { "name": "学", "pinyin": "xué", "pos": "verb", "meaning": "apprendre"},
            "中文": { "name": "中文", "pinyin": "zhōngwén", "pos": "noun", "meaning": "chinois(la langue)"},
            "汉语" : { "name": "汉语", "pinyin": "hànyǔ", "pos": "noun", "meaning": "mandarin(la langue)"},
            "韩语" : { "name": "韩语", "pinyin": "hányǔ", "pos": "noun", "meaning": "corean(la langue)"},
            "也" : { "name": "也", "pinyin": "yě", "pos": "adverb", "meaning": "aussi"},
            "吗" : { "name": "吗", "pinyin": "ma", "pos": "particule", "meaning": "particule interogative", "root":"口"},
            "姓" : { "name": "姓", "pinyin": "xìng", "pos": "verb", "meaning": "appeler (nom de famille)"},
            "什么" : { "name": "什么", "pinyin": "shénme", "pos": "adjectiv", "meaning": "quoi"},
            "李" : { "name": "李", "pinyin": "lǐ", "pos": "-", "meaning": "racine de prune (lizi) et nom de famille (Lee)"},
            "王" : { "name": "王", "pinyin": "Wáng", "pos": "noun", "meaning": "roi et nom de famille (wang)"},
            "张" : { "name": "张", "pinyin": "Zhāng", "pos": "-", "meaning": "nom de famille (Zhāng)"},
            "弓" : { "name": "弓", "pinyin": "gōng", "pos": "noun", "meaning": "arc, et nom de famille (Gōng)"},
            "叫" : { "name": "叫", "pinyin": "jiào", "pos": "verb", "meaning": "appeler (nom complet)"},
            "名字" : { "name": "名字", "pinyin": "míngzì", "pos": "noun", "meaning": "prenom"},
            "成龙" : { "name": "成龙", "pinyin": "chénglóng", "pos": "proper noun", "meaning": "Jackie Chan"},
            "龙" : { "name": "龙", "pinyin": "lóng", "pos": "noun", "meaning": "dragon"},
            "哪" : { "name": "哪", "pinyin": "nǎ", "pos": "adjectif", "meaning": "quel/lequel"},
            "国人" : { "name": "国人", "pinyin": "guórén", "pos": "noun", "meaning": "pays"},
            "大"  : { "name": "大", "pinyin": "dà", "pos": "adjectiv", "meaning": "grand"},
            "羊"  : { "name": "羊", "pinyin": "yáng", "pos": "noun", "meaning": "mouton"},
            "山羊"  : { "name": "山羊", "pinyin": "shānyáng", "pos": "noun", "meaning": "chevre (mouton des montagnes)"},
            "美"  : { "name": "美", "pinyin": "měi", "pos": "adjectiv", "meaning": "beau/jolie"},
            "很"  : { "name": "很", "pinyin": "hěn", "pos": "adverb", "meaning": "tres"},
            "小"  : { "name": "小", "pinyin": "xiǎo", "pos": "adjectiv", "meaning": "petit/jeune"},
            "矮"  : { "name": "矮", "pinyin": "ǎi", "pos": "adjectiv", "meaning": "petit(taille, utiliser sur les humain?)"},
            "高"  : { "name": "高", "pinyin": "gāo", "pos": "adjectiv", "meaning": "grand(taille)"},
            "老"  : { "name": "老", "pinyin": "lǎo", "pos": "adjectiv", "meaning": "vieux"},
            "老师"  : { "name": "老师", "pinyin": "lǎoshī", "pos": "noun", "meaning": "professeur"},
            "天安门"  : { "name": "天安门", "pinyin": "tiān'ānmén", "pos": "proper noun", "meaning": "Place Tiananmen"},
            "先生"  : { "name": "先生", "pinyin": "xiānshēng", "pos": "noun", "meaning": "monsieur"},
            "天"  : { "name": "天", "pinyin": "tiān", "pos": "noun", "meaning": "journée/ciel"},
            "好"  : { "name": "好", "pinyin": "hǎo", "pos": "adverb", "meaning": "bien"},
            "岁"  : { "name": "岁", "pinyin": "suì", "pos": "noun", "meaning": "age"},
            "去"  : { "name": "去", "pinyin": "qù", "pos": "verb", "meaning": "aller"},
            "来"  : { "name": "来", "pinyin": "lái", "pos": "verb", "meaning": "venir"},
            "第"  : { "name": "第", "pinyin": "dì", "pos": "-", "meaning": "prefix de position (premier diyi, deuxieme dier, etc..."},
            "小羊"  : { "name": "小羊", "pinyin": "xiǎoyáng", "pos": "noun", "meaning": "agneau (petit mouton)"},
            "小牛"  : { "name": "小牛", "pinyin": "xiǎo niú", "pos": "noun", "meaning": "veau (petite vache)"},
            "牛"  : { "name": "牛", "pinyin": "niú", "pos": "noun", "meaning": "vache"},
            "笑"  : { "name": "笑", "pinyin": "xiào", "pos": "noun", "meaning": "rire (nom et verbe"},
            "声"  : { "name": "声", "pinyin": "shēng", "pos": "noun", "meaning": "ton (son)"},
            "了"  : { "name": "了", "pinyin": "le", "pos": "particule", "meaning": "Particule, action passé"},
            "子"  : { "name": "子", "pinyin": "zi", "pos": "noun", "meaning": "enfant (fils)"},
            "汉字"  : { "name": "汉字", "pinyin": "hànzì", "pos": "noun", "meaning": "caractère chinois (glyphe)"},
            "书"  : { "name": "书", "pinyin": "shū", "pos": "noun", "meaning": "livre"},
            "法语" : { "name": "法语", "pinyin": "fǎyǔ", "pos": "adjectiv", "meaning": "français"},
            "有" : { "name": "有", "pinyin": "yǒu", "pos": "verb", "meaning": "avoir"},
            "本" : { "name": "本", "pinyin": "běn", "pos": "quantifier", "meaning": "quantifieur pour les livres"},
            "云" : { "name": "云", "pinyin": "yún", "pos": "noun", "meaning": "nuage"},
            "风" : { "name": "风", "pinyin": "fēng", "pos": "noun", "meaning": "vent"},
            "北京" : { "name": "北京", "pinyin": "běijīng", "pos": "proper noun", "meaning": "Pekin"},
            "北":  { "name": "北", "pinyin": "běi", "pos": "noun", "meaning": "nord"},
            "南":  { "name": "南", "pinyin": "nán", "pos": "noun", "meaning": "sud"},
            "京":  { "name": "京", "pinyin": "jīng", "pos": "noun", "meaning": "capital (d'un pays)"},
            "女儿": { "name": "女儿", "pinyin": "nǚ'ér", "pos": "noun", "meaning": "fille (feminin de fils)"},
            "儿子": { "name": "儿子", "pinyin": "érzi", "pos": "noun", "meaning": "fils (masculin de fille)"},
            "公司": { "name": "公司", "pinyin": "gōngsī", "pos": "noun", "meaning": "société/entreprise"},
            "咖啡": { "name": "咖啡", "pinyin": "kāfēi", "pos": "noun", "meaning": "café"},
            "身体": { "name": "身体", "pinyin": "shēntǐ", "pos": "noun", "meaning": "le corps/la santé"},
            "妈妈": { "name": "妈妈", "pinyin": "māmā", "pos": "noun", "meaning": "maman/mère", "root":"女"},
            "谢谢": { "name": "谢谢", "pinyin": "xièxiè", "pos": "noun", "meaning": "merci"},
            "不": { "name": "不", "pinyin": "bù", "pos": "adverb", "meaning": "negation (ne pas)"},
            "不客气": { "name": "不客气", "pinyin": "bù kèqì", "pos": "adverb", "meaning": "de rien/je vous en prie (fixer le ton XXX)"},
            "客人": { "name": "客人", "pinyin": "kèrén", "pos": "noun", "meaning": "invités/clients"},
            "休息": { "name": "休息", "pinyin": "xiūxí", "pos": "verb", "meaning": "se reposer"},
            "牛奶": { "name": "牛奶", "pinyin": "niúnǎi", "pos": "noun", "meaning": "lait (de vache)"},
            "啤酒": { "name": "啤酒", "pinyin": "píjiǔ", "pos": "noun", "meaning": "bière"},
            "米酒": { "name": "米酒", "pinyin": "mǐjiǔ", "pos": "noun", "meaning": "saké (alcool de riz)"},
            "大米": { "name": "大米", "pinyin": "dàmǐ", "pos": "noun", "meaning": "riz (la plante?XXX)"},
            "米饭": { "name": "米饭", "pinyin": "mǐfàn", "pos": "noun", "meaning": "riz (le plat)"},
            "梅酒": { "name": "梅酒", "pinyin": "méijiǔ", "pos": "noun", "meaning": "vin de prune"},
            "酒": { "name": "酒", "pinyin": "jiǔ", "pos": "noun", "meaning": "boisson alcolisée","root":"水"},
            "鸡蛋": { "name": "鸡蛋", "pinyin": "jīdàn", "pos": "noun", "meaning": "oeuf"},
            "红酒": { "name": "红酒", "pinyin": "hóngjiǔ", "pos": "noun", "meaning": "vin rouge"},
            "足球": { "name": "足球", "pinyin": "zúqiú", "pos": "noun", "meaning": "football"},
            "网球": { "name": "网球", "pinyin": "wǎngqiú", "pos": "noun", "meaning": "tennis"},
            "榴莲": { "name": "榴莲", "pinyin": "Liúlián", "pos": "noun", "meaning": "Durian"},
            "打开": { "name": "打开", "pinyin": "dǎkāi", "pos": "verb", "meaning": "allumer/ouvrir(un livre)"},
            "大声": { "name": "大声", "pinyin": "dàshēng", "pos": "adjectiv", "meaning": "Bruyamment"},
            "读": { "name": "读", "pinyin": "dú", "pos": "verb", "meaning": "lire/prononcer"},
            "请": { "name": "请", "pinyin": "qǐng", "pos": "-", "meaning": "SVP"},
            "问": { "name": "问", "pinyin": "wèn", "pos": "verb", "meaning": "demander"},
            "请问": { "name": "请问", "pinyin": "qǐngwèn", "pos": "-", "meaning": "excusez moi"},
            "再见": { "name": "再见", "pinyin": "zàijiàn", "pos": "-", "meaning": "au revoir"},
            "一起": { "name": "一起", "pinyin": "yīqǐ", "pos": "-", "meaning": "ensemble"},
            "对不起": { "name": "对不起", "pinyin": "duìbùqǐ", "pos": "-", "meaning": "désolé"},
            "问题": { "name": "问题", "pinyin": "wèntí", "pos": "noun", "meaning": "problème/question"},
            "个": { "name": "个", "pinyin": "gè", "pos": "quantifier", "meaning": "quantifieur generique"},
            "没": { "name": "没", "pinyin": "méi", "pos": "-", "meaning": "negation (pour avoir)", "root":"水"},
            "学生": { "name": "学生", "pinyin": "xuéshēng", "pos": "noun", "meaning": "etudiant (eleve)"},
            "大学生": { "name": "大学生", "pinyin": "dàxuéshēng", "pos": "noun", "meaning": "etudiant (haute-etude)"},
            "小学生": { "name": "小学生", "pinyin": "xiǎoxuéshēng", "pos": "noun", "meaning": "élève de primaire"},
            "中学生": { "name": "中学生", "pinyin": "zhōngxuéshēng", "pos": "noun", "meaning": "collégien"},
            "哪儿": { "name": "哪儿", "pinyin": "nǎ'er", "pos": "particule", "meaning": "où"},
            "这": { "name": "这", "pinyin": "zhè", "pos": "-", "meaning": "Ceci (this)"},
            "那": { "name": "那", "pinyin": "nà", "pos": "-", "meaning": "Cela (that)"},
            "巴黎": { "name": "巴黎", "pinyin": "bālí", "pos": "proper noun", "meaning": "Paris"},
            "天空": { "name": "天空", "pinyin": "tiānkōng", "pos": "noun", "meaning": "ciel"},
            "都": { "name": "都", "pinyin": "dōu", "pos": "adverb", "meaning": "tout"},
            "明天": { "name": "明天", "pinyin": "míngtiān", "pos": "noun", "meaning": "demain"},
            "木": { "name": "木", "pinyin": "mù", "pos": "noun", "meaning": "bois/arbre"},
            "目": { "name": "目", "pinyin": "mù", "pos": "noun", "meaning": "oeil"},
            "中": { "name": "中", "pinyin": "zhōng", "pos": "adjectiv", "meaning": "central (au milieux)"},
            "坏": { "name": "坏", "pinyin": "huài", "pos": "adjectiv", "meaning": "mal/mauvais"},
            "男": { "name": "男", "pinyin": "nán", "pos": "adjectiv", "meaning": "masculin"},
            "朋友": { "name": "朋友", "pinyin": "péngyǒu", "pos": "noun", "meaning": "ami"},
            "今天": { "name": "今天", "pinyin": "jīntiān", "pos": "noun", "meaning": "aujourdh'ui"},
            "月": { "name": "月", "pinyin": "yuè", "pos": "noun", "meaning": "mois/lune"},
            "号": { "name": "号", "pinyin": "hào", "pos": "noun", "meaning": "jour/nombre (3eme jour)"},
            "星期": { "name": "星期", "pinyin": "xīngqī", "pos": "noun", "meaning": "la semaine"},
            "昨天": { "name": "昨天", "pinyin": "zuótiān", "pos": "noun", "meaning": "demain"},
            "下个": { "name": "下个", "pinyin": "xiàgè", "pos": "noun", "meaning": "le prochain (celui d'après)"},
            "上个": { "name": "上个", "pinyin": "shànggè", "pos": "noun", "meaning": "le precedent (celui d'avant)"},
            "这个": { "name": "这个", "pinyin": "zhègè", "pos": "noun", "meaning": "celui-la (celui en cours)"},
            "生日": { "name": "生日", "pinyin": "shēngrì", "pos": "noun", "meaning": "anniversaire"},
            "的": { "name": "的", "pinyin": "de", "pos": "noun", "meaning": "(defini l'appartenance) nide->a toi"},
            "情人": { "name": "情人", "pinyin": "qíngrén", "pos": "noun", "meaning": "amoureux/amant"},
            "节": { "name": "节", "pinyin": "jié", "pos": "noun", "meaning": "(suffix pour les fetes)"},
            "节日": { "name": "节日", "pinyin": "jiéri", "pos": "noun", "meaning": "jour de fete"},
            "星期天": { "name": "星期天", "pinyin": "xīngqítiān", "pos": "noun", "meaning": "dimanche (jour du ciel)"},
            "星期日": { "name": "星期日", "pinyin": "xīngqírì", "pos": "noun", "meaning": "dimanche (jour du soleil)"},
            "周": { "name": "周", "pinyin": "zhōu", "pos": "noun", "meaning": "la semaine (ecrit/soutenu)"},
            "周末": { "name": "周末", "pinyin": "zhōumò", "pos": "noun", "meaning": "weekend"},
            "愉快": { "name": "愉快", "pinyin": "yúkuài", "pos": "adjectiv", "meaning": "joyeux"},
            "多少": { "name": "多少", "pinyin": "duōshǎo", "pos": "noun", "meaning": "combien"},
            "高兴": { "name": "高兴", "pinyin": "gāoxìng", "pos": "noun", "meaning": "content"},
            "假期": { "name": "假期", "pinyin": "jiàqī", "pos": "noun", "meaning": "vacances"},
            "学校": { "name": "学校", "pinyin": "xuéxiào", "pos": "noun", "meaning": "ecole"},
            "医院": { "name": "医院", "pinyin": "yīyuàn", "pos": "noun", "meaning": "hopital"},
            "看书": { "name": "看书", "pinyin": "kànshū", "pos": "verb", "meaning": "lire (regarder un livre)"},        
            "做": { "name": "做", "pinyin": "zuò", "pos": "verb", "meaning": "faire"},
            "家": { "name": "家", "pinyin": "jiā", "pos": "adverb", "meaning": "(pour)"},
            "地方": { "name": "地方", "pinyin": "dìfāng", "pos": "nom", "meaning": "lieu/endroit"},
            "商店": { "name": "商店", "pinyin": "shāngdiàn", "pos": "nom", "meaning": "magasin/boutique"},
            "会说": { "name": "会说", "pinyin": "huì shuō", "pos": "verb", "meaning": "pouvoir parler (une langue)"},
            "儿德语": { "name": "儿德语", "pinyin": "er déyǔ", "pos": "noun", "meaning": "allemand"},
            "一点": { "name": "一点", "pinyin": "yīdiǎn", "pos": "adverb", "meaning": "un peu"},
            "俄语": { "name": "俄语", "pinyin": "èyǔ", "pos": "noun", "meaning": "russe"},
            "俄国": { "name": "俄语", "pinyin": "èyǔ", "pos": "noun", "meaning": "Russie"},
            "俄罗斯": { "name": "俄罗斯", "pinyin": "èluósī", "pos": "noun", "meaning": "Russie"},
            "怎么": { "name": "怎么", "pinyin": "zěnme", "pos": "adverb", "meaning": "comment"},
            "吃": { "name": "吃", "pinyin": "chī", "pos": "verb", "meaning": "manger", "root":"口"},
            "菜": { "name": "菜", "pinyin": "cài", "pos": "noun", "meaning": "plat"},
            "写": { "name": "写", "pinyin": "xiě", "pos": "verb", "meaning": "écrire"},
            "意思": { "name": "意思", "pinyin": "yìsi", "pos": "noun", "meaning": "sens (meaning)"},
            "有意思": { "name": "有意思", "pinyin": "yǒuyìsi", "pos": "adjectiv", "meaning": "interessant (avoir du sens)"},
            "时间": { "name": "时间", "pinyin": "shíjiān", "pos": "noun", "meaning": "temp (time)"},
            "回": { "name": "回", "pinyin": "huí", "pos": "verb", "meaning": "revenir (à la maison/au pays)"},
            "三点水": { "name": "三点水", "pinyin": "sān diǎn shuǐ", "pos": "-", "meaning": "la clef/racine de l'eau (3 point)"},
            "海": { "name": "海", "pinyin": "hǎi", "pos": "noun", "meaning": "mer","root":"水"},
            "河": { "name": "河", "pinyin": "hé", "pos": "noun", "meaning": "riviere","root":"水"},
            "开门": { "name": "开门", "pinyin": "kāimén", "pos": "verb", "meaning": "ouvrir (porte ouverte)"},
            "银行": { "name": "银行", "pinyin": "yínháng", "pos": "noun", "meaning": "banque"},
            "沉没": { "name": "沉没", "pinyin": "chénmò", "pos": "noun", "meaning": "evier"},
            "法": { "name": "法", "pinyin": "fǎ", "pos": "noun", "meaning": "lois","root":"水"},
            "汉": { "name": "汉", "pinyin": "hàn", "pos": "noun", "meaning": "chinois","root":"水"},
            "开": { "name": "开", "pinyin": "kāi", "pos": "verb", "meaning": "ouvrir"},
            "言": { "name": "言", "pinyin": "yán", "pos": "verb", "meaning": "parler"},
            "言语": { "name": "言语", "pinyin": "yányǔ", "pos": "noun", "meaning": "discours"},
            "说": { "name": "说", "pinyin": "shuō", "pos": "verb", "meaning": "parler","root":"言"},
            "谁": { "name": "谁", "pinyin": "shéi", "pos": "-", "meaning": "qui","root":"言"},
            "让": { "name": "让", "pinyin": "ràng", "pos": "verb", "meaning": "laisser","root":"言"},
            "认": { "name": "认", "pinyin": "rèn", "pos": "verb", "meaning": "reconnaitre","root":"言"},
            "认识": { "name": "认识", "pinyin": "rènshí", "pos": "verb", "meaning": "connaitre/savoir"},
            "玩": { "name": "玩", "pinyin": "wán", "pos": "noun", "meaning": "jeux/jouet", "root":"王"},
            "字": { "name": "字", "pinyin": "zì", "pos": "noun", "meaning": "mots (word)"},
            "呢": { "name": "呢", "pinyin": "ne", "pos": "adjectiv", "meaning": "quoi"},
            "喝": { "name": "喝", "pinyin": "hē", "pos": "verb", "meaning": "boire"},
            "怕": { "name": "怕", "pinyin": "pà", "pos": "verb", "meaning": "avoir peur"},
            "孩子": { "name": "孩子", "pinyin": "háizi", "pos": "noun", "meaning": "enfant"},
            "医生": { "name": "医生", "pinyin": "yīshēng", "pos": "noun", "meaning": "médecin"},
            "姐姐": { "name": "姐姐", "pinyin": "jiějiě", "pos": "noun", "meaning": "soeur", "root":"女"},
            "妹妹": { "name": "妹妹", "pinyin": "mèimei", "pos": "noun", "meaning": "soeur cadette", "root":"女"},
            "饱了": { "name": "饱了", "pinyin": "bǎole", "pos": "??", "meaning": "bien mangé"},
            "青菜": { "name": "青菜", "pinyin": "qīngcài", "pos": "noun", "meaning": "legume (plat vert)"},
            "草": { "name": "草", "pinyin": "cǎo", "pos": "noun", "meaning": "herbe"},
            "芳": { "name": "芳", "pinyin": "fāng", "pos": "noun", "meaning": "senteur parfumé"},
            "日": { "name": "日", "pinyin": "rì", "pos": "noun", "meaning": "journée/soleil"},
            "见": { "name": "见", "pinyin": "rì", "pos": "verb", "meaning": "voir"},
            "下": { "name": "下", "pinyin": "Xià", "pos": "adjectiv", "meaning": "en dessous/prochain"},
        },
    "sentence":
        [
            {
                "chinese" : "下|周|一|见",
                "french": "On se voit lundi prochain",
                "seance" : 11
            },
            
            {
                "chinese" : "我|吃|饱了",
                "french": "J'ai bien mangé",
                "seance" : 11
            },
            {
                "chinese" : "孩子|怕|去|医院",
                "french": "L'enfant a peur d'aller à l'hopital",
                "seance" : 11
            },
            {
                "chinese" : "很|高兴|认识|你",
                "french": "Ravi de vous rencontrer",
                "seance" : 11
            },
            {
                "chinese" : "玩|，|这个|字|你们|认识|吗",
                "french": "玩, vous connaissez ce mot?",
                "seance" : 11
            },
            {
                "chinese" : "我|认识|这个|学生",
                "french": "Je connais cet élève",
                "seance" : 11
            },
            {
                "chinese" : "妈妈|让|女儿|去|商店",
                "french": "Maman laisse sa fille aller au magasin",
                "seance" : 11
            },
            {
                "chinese" : "老师|让|学生|说|汉语",
                "french": "Le professeur laisse les eleves parler chinois",
                "seance" : 11
            },
            {
                "chinese" : "银行|昨天|没|开门",
                "french": "La banque n'etait pas ouverte hier",
                "seance" : 11
            },
            {
                "chinese" : "什么|意思",
                "french": "Quel en ai le sens?",
                "seance" : 11
            },
            {
                "chinese" : "怎么|写",
                "french": "Comment (l')ecrire",
                "seance" : 11
            },
            {
                "chinese" : "这个|汉字|怎么|读",
                "french": "Comment prononcer ce caratere chinois",
                "seance" : 11
            },
            {
                "chinese" : "你们|会说|俄语",
                "french": "Vous pouvez parler russe",
                "seance" : 11
            },
            {
                "chinese" : "我|会说|一点|儿德语",
                "french": "Je parle un peu allemand",
                "seance" : 11
            },
            {
                "chinese" : "什么|地方",
                "french": "Quel lieu?",
                "seance" : 11
            },
            {
                "chinese" : "明天|我|去|中国|朋友|家|吃|中国|菜",
                "french": "Demain j'irai chez un ami chinois pour manger de la nourriture chinoise",
                "seance" : 11
            },
            {
                "chinese" : "明天|星期|六|，|你|做|什么",
                "french": "Que fais tu demain samedi",
                "seance" : 11
            },
            {
                "chinese" : "假期|愉快",
                "french": "Bonne vacances",
                "seance" : 11
            },
            {
                "chinese" : "节日|愉快",
                "french": "Joyeuse fete",
                "seance" : 11
            },
            {
                "chinese" : "周末|愉快",
                "french": "Bon weekend",
                "seance" : 11
            },
            {
                "chinese" : "情人|节|是|2|月|14|号",
                "french": "La saint Valentin est le 14 fevrier",
                "seance" : 11
            },
            {
                "chinese" : "你|的|生日|是|几|月|几|号",
                "french": "Quand est ton anniverssaire (jour/mois)",
                "seance" : 11
            },
            {
                "chinese" : "这个|月|有|多少|天",
                "french": "Combien de jour il y a dans le mois en cours",
                "seance" : 11
            },
            {
                "chinese": "我|五|岁",
                "french": "J'ai 5 ans"
            },
            {
                "chinese": "你|几|岁",
                "french": "Tu as quel age?"
            },
            {
                "chinese" : "我们|学|中文",
                "french": "Nous apprenons le chinois",
                "seance" : 1
            },
            {
                "chinese" : "我们|学|汉语",
                "french": "Nous apprenons le Mandarin",
                "seance" : 1
            },
            {
                "chinese" : "我们|学|韩语",
                "french": "Nous apprenons le Coreen",
                "seance" : 1
            },
            {
                "chinese" : "你们|是|法国|人",
                "french": "Vous êtes français",
                "seance" : 1
            },
            {
                "chinese" : "他们|也|学|中文|吗",
                "french": "Apprennent-ils aussi le chinois",
                "seance" : 1
            },
            {
                "chinese" : "你|姓|什么",
                "french": "Quel est votre nom de famille?",
                "seance" : 1
            },
            {
                "chinese" : "我|姓|李",
                "french": "Mon nom de famille est Li",
                "seance" : 1
            },
            {
                "chinese" : "你|叫|什么",
                "french": "Quel est votre nom?",
                "seance" : 1
            },
            {
                "chinese" : "你|叫|什么|名字",
                "french": "Comment tu t'appele (nom complet)",
                "seance" : 1
            },
            {
                "chinese" : "你|是|哪|国人",
                "french": "De quel pays êtes vous?",
                "seance" : 1
            },
            {
                "chinese" : "我|是|中国|人",
                "french": "je suis Chinois",
                "seance" : 1
            },
            {
                "chinese" : "法国|很|美",
                "french": "La France est belle (verbe pas obligatoire)",
                "seance" : 1
            },
            {
                "chinese" : "我|很|好",
                "french": "Je vais bien",
                "seance" : 1
            },
            {
                "chinese" : "她|很|小",
                "french": "Elle est très petite",
                "seance" : 1
            },
            {
                "chinese" : "中国|很|大|，|法国|很|小",
                "french": "La Chine est très grande, la France est très petite",
                "seance" : 1
            },
            {
                "chinese" : "他|很|矮",
                "french": "Il est petit",
                "seance" : 1
            },
            {
                "chinese" : "她|去|中国",
                "french": "Elle va en Chine",
                "seance" : 2
            },
            {
                "chinese" : "你|来|吗",
                "french": "Tu viens?",
                "seance" : 2
            },
            {
                "chinese" : "第|几|声",
                "french": "C'est quel ton",
                "seance" : 2
            },
            {
                "chinese" : "第|二|声",
                "french": "C'est le 2eme ton",
                "seance" : 2
            },
            {
                "chinese" : "她|笑|了",
                "french": "Elle a rie",
                "seance" : 2
            },
            {
                "chinese" : "我|有|一|本|汉语|书",
                "french": "J'ai un livre chinois",
                "seance" : 2
            },
            {
                "chinese" : "你|身体|好|吗",
                "french": "Tu va bien?",
                "seance" : 2
            },
            {
                "chinese" : "你|妈妈|身体|好|吗",
                "french": "Ta maman va bien?",
                "seance" : 2
            },
            {
                "chinese" : "你|好|吗",
                "french": "Tu va bien?",
                "seance" : 2
            },
            {
                "chinese" : "她|不|去|中国",
                "french": "Elle n'ira pas en Chine",
                "seance" : 2
            },
            {
                "chinese" : "请问|您|是|法国|人|吗",
                "french": "Puis-je vous demander si vous etes francais?",
                "seance" : 2
            },
            {
                "chinese" : "请问|你|叫|什么|名字",
                "french": "Puis-je vous demander quel est votre nom?",
                "seance" : 2
            },
            {
                "chinese" : "一起|读",
                "french": "Lisez ensemble",
                "seance" : 2
            },
            {
                "chinese" : "你们|一起|学|汉语",
                "french": "Vous apprenez le chinois ensemble",
                "seance" : 2
            },
            {
                "chinese" : "我|有|一|个|问题",
                "french": "J'ai une question",
                "seance" : 2
            },
            {
                "chinese" : "她|有|一|个|儿子",
                "french": "Elle a un fils",
                "seance" : 2
            },
            {
                "chinese" : "你|有|汉语|书|吗",
                "french": "Avez vous un livre de chinois",
                "seance" : 2
            },
            {
                "chinese" : "您|是|老师|吗",
                "french": "Etes vous professeur?",
                "seance" : 2
            },
            {
                "chinese" : "是|，|我|是|老师",
                "french": "Oui, je suis professeur",
                "seance" : 2
            },
            {
                "chinese" : "不|，|我|不|是|老师|，|我|是|学生",
                "french": "Non, je ne suis pas professeur. Je suis étudiant.",
                "seance" : 2
            },
            {
                "chinese" : "你|是|法国|人|吗",
                "french": "Tu es français?",
                "seance" : 2
            },
            {
                "chinese" : "我|是|美国|人",
                "french": "Je suis Américain",
                "seance" : 2
            },
            {
                "chinese" : "这|是|哪儿",
                "french": "Où est-ce?",
                "seance" : 2
            },
            {
                "chinese" : "这|是|法国|巴黎",
                "french": "C'est Paris en France",
                "seance" : 2
            },
            {
                "chinese" : "两|个|都|是|第|四|声",
                "french": "Les deux sont quatrièmes tons",
                "seance" : 3
            },
            {
                "chinese" : "他|很|好",
                "french": "Il est bon",
                "seance" : 3
            },
            {
                "chinese" : "他|很|男|学生",
                "french": "Il est un étudiant masculin",
                "seance" : 3
            },
            {
                "chinese" : "你们|都|是|法国|人",
                "french": "Vous êtes tous français",
                "seance" : 3
            },
            {
                "chinese" : "巴黎|不|大",
                "french": "Paris n'est pas grand",
                "seance" : 3
            },
            {
                "chinese" : "我|有|法国|朋友",
                "french": "J'ai des amis français",
                "seance" : 3
            },
            {
                "chinese" : "我|有|中文|名字",
                "french": "J'ai un nom chinois",
                "seance" : 3
            },
            {
                "chinese" : "今天|几|月|几|号",
                "french": "Quel jour sommes-nous aujourd'hui? (jour et mois)",
                "seance" : 11
            },
            {
                "chinese" : "今天|1|月|18|号",
                "french": "Aujourdh'ui, nous sommes le 18 janvier",
                "seance" : 11
            },
            {
                "chinese" : "今天|星期|几",
                "french": "Quel jour sommes-nous aujourd'hui? (jour de la semaine)",
                "seance" : 11
            },
            {
                "chinese" : "今天|星期|一",
                "french": "Aujourdh'ui on est lundi",
                "seance" : 11
            },
            {
                "chinese" : "昨天|星期|天",
                "french": "Hier on etait dimanche (le jour du ciel?)",
                "seance" : 11
            },
            {
                "chinese" : "明天|是|不|是|星期|三",
                "french": "Est-ce que demain on est mercredi",
                "seance" : 11
            },
            {
                "chinese" : "不|是|，|明天|星期|二",
                "french": "Non, demain c'est mardi",
                "seance" : 11
            },
            {
                "chinese" : "明天|是|20|号|吗",
                "french": "Est-ce qu'on est le 20 demain?",
                "seance" : 11
            },
            {
                "chinese" : "不|是|，|明天|是|19|号",
                "french": "Non, demain on est le 19",
                "seance" : 11
            },
            {
                "chinese" : "下个|星期|五|是|几|号",
                "french": "Quel jour est le prochain vrendredi",
                "seance" : 11
            },
            
            
            
            
            
            
            
            
            
            
        ]
}