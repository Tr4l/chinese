import React, { Component } from "react";
import { contentData } from "./data";
import ReactCardFlip from 'react-card-flip';

class Sentence extends Component {
    state = {
        french: "",
        chinese: [],
        content: ""
    };
    constructor(props) {
        super(props);
        this.state = {
            isFlipped: false
        };
        var french = this.props.sentence.french;
        this.state.french = french;
        var chinese = [];
        var wordChinese = this.props.sentence.chinese.split("|");
        for (var i = 0; i < wordChinese.length; i++) {
            var word = {
                word: wordChinese[i],
                title: wordChinese[i]
            }
            if (contentData.words[wordChinese[i]]) {
                word.word = contentData.words[wordChinese[i]].name;
                word.pos = contentData.words[wordChinese[i]].pos;
                word.title = contentData.words[wordChinese[i]].pinyin;
                word.meaning = contentData.words[wordChinese[i]].meaning;
            }else if (wordChinese[i] !== "，"){
                console.log("Missing word:" + wordChinese[i])
            }
            chinese.push(word);
        }
        this.state.chinese = chinese;
        this.state.content = wordChinese.join();
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(e) {
        e.preventDefault();
        this.setState(prevState => ({ isFlipped: !prevState.isFlipped }));
    }
    handleListenClick(e) {
        e.stopPropagation();
        var msg = new SpeechSynthesisUtterance();
        msg.text = e.target.dataset.sound;
        msg.lang = 'zh';
        window.speechSynthesis.speak(msg);
    }
    render() {
        return (
            <div class="cards" onClick={this.handleClick}>

                <ReactCardFlip isFlipped={this.state.isFlipped} flipDirection="vertical">


                    <div class="card">
                        <div class="cardcontent">
                            <span class="sentence">
                                {this.state.chinese.map((data) => (
                                    <><abbr class={data.pos} title={data.title}>{data.word}</abbr><wbr /></>
                                ))}
                                <br />
                                {this.state.chinese.map((data) => (
                                    <><abbr class={data.pos} title={data.meaning}>{data.title}</abbr> <wbr /></>
                                ))}

                            </span>
                        </div>
                        <div class="pron" onClick={this.handleListenClick} data-sound={this.state.content}>🔈</div>
                    </div>
                    <div class="card">
                        <div class="cardcontent">
                            {this.state.french}
                        </div>
                    </div>
                </ReactCardFlip>

            </div>
        );
    }
}

export default Sentence;