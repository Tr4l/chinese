import React, { Component } from "react";
import "./App.css";
import { contentData } from "./data";
import Sentence from "./Sentence";
import Word from "./Word";
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';


class Content extends Component {
    state = {
        sentences: [],
        words: [],
        sceance: ["1","2","3","11"]
    };
    constructor(props) {
        super(props);
        this.sf = this.sf.bind(this);
        this.state.sentences = contentData.sentence;//.filter(this.sf);
        this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
        //const result = words.filter(word => word.length > 6);
    }
    handleCheckboxChange = (event: any) => {
        const { name } = event.target;
        console.log(name);
        const checkedArr = [];
        let value;
        const checkeds = document.getElementsByName(name);
        for (let i = 0; i < checkeds.length; i++) {
            if (checkeds[i].checked) {
                checkedArr.push(checkeds[i].value);
            }
        }
        value = checkedArr;
        console.log(value);
        this.setState({ [name]: value });
        this.setState({"sentences": contentData.sentence.filter(this.sf)});
    };    
    sf(curr) {
        if (this.state.sceance.includes(""+curr.seance)) return true;
        return false;
    }
    render() {
        return (
            <>
                <Tabs>
                    <TabList>
                        <Tab>Sentence</Tab>
                        <Tab>Words</Tab>
                    </TabList>

                    <TabPanel>
                            {/*<div>Sceance: 
                                {["1","2","3","11"].map((type) => (
                                    <>
                                    <label>{type}</label><input type="checkbox" name="sceance" value={type} label={type} onChange={this.handleCheckboxChange} checked={this.state.sceance.includes(type)} />
                                    </>
                                ))}
                                </div>*/}
                        {this.state.sentences.map((data, key) => {
                            return (
                                <div key={key}>
                                    <Sentence sentence={data} />
                                </div>
                            );
                        })}
                    </TabPanel>
                    <TabPanel>
                        {Object.keys(contentData.words).map((data, key) => {
                            return (
                                <div key={key}>
                                    <Word word={contentData.words[data]} />
                                </div>
                            );
                        })}
                    </TabPanel>
                </Tabs>
            </>
        );
    }
}

export default Content;