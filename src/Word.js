import React, { Component } from "react";
import ReactCardFlip from 'react-card-flip';

class Word extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isFlipped: false
        };

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(e) {
        e.preventDefault();
        this.setState(prevState => ({ isFlipped: !prevState.isFlipped }));
    }
    handleListenClick(e) {
        e.stopPropagation();
        var msg = new SpeechSynthesisUtterance();
        msg.text = e.target.dataset.sound;
        msg.lang = 'zh';
        window.speechSynthesis.speak(msg);
    }

    render() {
        return (
            <div class="cards" onClick={this.handleClick}>
                <ReactCardFlip isFlipped={this.state.isFlipped} flipDirection="vertical">
                    <div class="card">
                        <div class="cardcontent">
                            <span class="word">
                                <abbr class={this.props.word.pos} title={this.props.word.pinyin}>{this.props.word.name}</abbr>
                                
                            </span>
                        </div>
                        <div class="pron" onClick={this.handleListenClick} data-sound={this.props.word.name}>🔈</div>
                    </div>
                    <div class="card">
                        <div class="cardcontent">
                            {this.props.word.meaning}<br/>
                            {this.props.word.pinyin}
                        </div>
                    </div>
                </ReactCardFlip>
            </div>
        );
    }
}

export default Word;